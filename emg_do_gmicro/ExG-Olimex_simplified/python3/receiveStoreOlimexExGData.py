#!/usr/bin/env python3

## +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
## DEVELOPER: Cesar Abascal
## PROFESSORS: Cesar Augusto Prior and Cesar Rodrigues (Yeah. Its almost an overflow!)
## PROJECT: Olimex EKG/EMG - Eletrocardiogram/Eletromiogram waves acquisition
## ARCHIVE: Simple script to acquire data.
## DATE: 12/03/2019
## +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


## LIBRARIES ----------------------------------------------------------------------------
import os
import serial

# CONSTANTS -----------------------------------------------------------------------------
# Channels, sync and baudrate
NUMCHANNELS = 6
HEADERLEN = 4
PACKAGE_SIZE = ((NUMCHANNELS*2)+HEADERLEN+1)
SYNC0 = b'\xa5' # 0xa5, b'\xa5', 165
SYNC1 = b'Z'    # 0x5a, b'Z', 90
DEFAULT_BAUDRATE = 57600 # To use in serial communication

# Olimex ExG package protocol
OLIMEX_PACKAGE = {
    'sync0': slice(0, 1),
    'sync1': slice(1, 2),
    'version': slice(2, 3),
    'count': slice(3, 4),
    'data': slice(4, 16),
    'switches': slice(16)
}

# comm port and file
commPort = "/dev/cu.usbmodem1414101" # PUT YOUR SERIAL PORT HERE.

# SCRIPT FUNCTIONS ----------------------------------------------------------------------
# Calculates the values ​​received by the olimex byte packet.
def calculateValuesFromDataPackage(data):
    values = []

    for index in range(0, len(data), 2):
        # byte_a is the most significant byte and byte_b is the least significant byte.
        byte_a, byte_b = data[index], data[index + 1]
        val = (byte_a << 8) | byte_b
        # Data comes in upside down.
        # Flip data around a horizontal axis.
        val = (val - 1024) * -1
        values.append(val)
    #end-for

    return values
#end-def

# MAIN ----------------------------------------------------------------------------------
print("\n| OLIMEX ExG |\n")

# Get patient name and create folder.
patientN = str(input("Patient name: "))
path = "exg-data/raw-exg/" + patientN
if(not os.path.exists(path)):
    os.makedirs(path)
# Create new csv file with header
dataFile = open(path+"/ExG_signals.csv", "w")
dataFile.write("ExG_CH1;ExG_CH2;ExG_CH3;ExG_CH4;ExG_CH5;ExG_CH6\n")

# Create a serial communication
print("Starting serial communication...\n")
ser = serial.Serial(commPort, DEFAULT_BAUDRATE)

# Script Loop
while(True):
    try:
        if (ser.inWaiting()):
            # If there is data, it receives the same.
            package = ser.read(PACKAGE_SIZE)

            # Split sync data basead on olimex package protocol
            recSync0 = package[OLIMEX_PACKAGE['sync0']]
            recSync1 = package[OLIMEX_PACKAGE['sync1']]

            if(recSync0 == SYNC0 and recSync1 == SYNC1):
                # If true, we have a correct package.
                # For now we'll ignore 'version package' and 'count package'. But it's important
                
                # Here we have the 12 EKG/EMG bytes data.
                exgData = package[OLIMEX_PACKAGE['data']]

                # Here we calculate the 6 channels data acquired on olimex.
                calculatedValues = calculateValuesFromDataPackage(exgData)

                # Here we store the 6 channel data on csv file.
                dataFile.write(str(calculatedValues[0]) + ";" + \
                                str(calculatedValues[1]) + ";" + \
                                str(calculatedValues[2]) + ";" + \
                                str(calculatedValues[3]) + ";" + \
                                str(calculatedValues[4]) + ";" + \
                                str(calculatedValues[5]) + "\n")
                
                print("Data recieved and stored. To stop, press Ctrl+C")
            #end-if
        #end-if
    #end-try
    
    except KeyboardInterrupt:
        # If someone press Crtl+C, close file, close serial comm and stop script.
        dataFile.close()
        ser.__del__()
        print("\nOLIMEX ExG Finished\n")
        break
    #end-execpt
#end-while